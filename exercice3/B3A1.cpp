#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;
double h = 0.01, m = 1;

double rpunkt(double t, double x, double v)
{
    double rpkt = v;
    return rpkt;
}

double vpunkt(double t, double x, double v)
{
	double f = -m*x; // Aufgabe 2 
    double vpkt = f/m;  
    return vpkt;
}

void rk4(double t, double x, double v, double rpunkt(double t, double x, double v), double vpunkt(double t, double x, double v), double* tneu, double* xneu, double* vneu)
{
	double k1r, k2r, k3r, k4r, k1v, k2v, k3v, k4v; // k*r und k*v fuer die 4-Schritt-Approximation
	k1r = h*rpunkt(t, x, v); 
	k1v = h*vpunkt(t, x, v);
	
	k2r = h*rpunkt(t+h/2.0, x+k1r/2.0, v+k1v/2.0);
	k2v = h*vpunkt(t+h/2.0, x+k1r/2.0, v+k1v/2.0);
	
	k3r = h*rpunkt(t+h/2.0, x+k2r/2.0, v+k2v/2.0);
	k3v = h*vpunkt(t+h/2.0, x+k2r/2.0, v+k2v/2.0);
	
	k4r = h*rpunkt(t+h, x+k3r, v+k3v);
	k4v = h*vpunkt(t+h, x+k3r, v+k3v);
	
	//cout << "x: " << x << endl;
	//cout << "v: " << v << endl;
	//cout << "xneu: " << x + (k1r + 2.0*(k2r+k3r) + k4r)/6.0 << endl;
	//cout << "vneu:" << v + (k1v + 2.0*(k2v+k3v) + k4v)/6.0 << endl;
	
	*xneu = x + (k1r + 2.0*(k2r+k3r) + k4r)/6.0;
	*vneu = v + (k1v + 2.0*(k2v+k3v) + k4v)/6.0;
	*tneu = t+h;
}


int main(){
	ofstream wegx("Wegx.txt"); // Auslenkung x
	ofstream wegy("Wegy.txt"); // Auslenkung y
	ofstream wegz("Wegz.txt"); // Auslenkung z	
	ofstream geschx("Geschwindigkeitx.txt"); // Geschwindigkeit x	
	ofstream geschy("Geschwindigkeity.txt"); // Geschwindigkeit x	
	ofstream geschz("Geschwindigkeitz.txt"); // Geschwindigkeit x			
	ofstream pot("PotEnergie.txt"); // Potentielle Energie
	ofstream kin("KinEnergie.txt"); // Kinetische Energie
	ofstream ges("GesEnergie.txt"); // Gesamte Energie 

	int d;
	cout << "Es wird die newtonsche Bewegungsgleichung eines Teilchens im Kraftfeld F(r)" << endl;
	cout << "mit Hilfe des Runge-Kutta-Verfahrens 4. Ordnung geloest." << endl,
	cout << "Dafuer bitte die zu betrachtende Dimension angeben: ";
	cin >> d; 
	cout << endl;
	
	if(d<3){
		cout << "Dimension ist kleiner als 3" << endl;
		return 0;
	}
	
	double tar[d][2]; // Erste: Dimension, Zweites: vorher/nachher Wert
	double xar[d][2];
	double var[d][2];

	double Ekin, Epot;
	cout << "Jetzt die " << d << "-dim. Anfangsbedingung (AB) fuer r angeben: " << endl;
	for(int i=0; i<d; i++)
	{
		cout << i+1 << ". AB:" << ": ";
		cin >> xar[i][0];	
	} 
	
	cout << endl;
	cout << "Jetzt die " << d << "-dim. Anfangsbedingung (AB) fuer v angeben: " << endl;
	for(int i=0; i<d; i++)
	{
		cout << i+1 << ". AB:" << ": ";
		cin >> var[i][0];	
	} 
	
	tar[0][0]=0;
	tar[1][0]=0;
	tar[2][0]=0;
	
	wegy << "# t x" << "\n";
	wegx << tar[0][0] << "\t" << xar[0][0] << "\n";
	wegy << "# t x" << "\n";
	wegy << tar[1][0] << "\t" << xar[1][0] << "\n";
	wegz << "# t x" << "\n";
	wegz << tar[2][0] << "\t" << xar[2][0] << "\n";
	geschx << "# t v" << "\n";
	geschx << tar[0][0] << "\t" << var[0][0] << "\n";
	geschy << "# t v" << "\n";
	geschy << tar[1][0] << "\t" << var[1][0] << "\n";
	geschz << "# t v" << "\n";
	geschz << tar[2][0] << "\t" << var[2][0] << "\n";
	
	for(int k = 0; k<d; k++)
	{
		Ekin += 0.5*m*pow(xar[k][0],2);
		Epot += 0.5*m*pow(var[k][0],2);
	}
	
	pot << "# t Epot" << "\n";
	pot << tar[0][0] << "\t" << Epot << "\n";
	kin << "# t Ekin" << "\n";
	kin << tar[0][0] << "\t" << Ekin << "\n";
	ges << "# t Eges" << "\n";
	ges << tar[0][0] << "\t" << Epot+Ekin << "\n";
	
	for(double step=0; step<10; step+=h){
		for(int k=0; k<d; k++)
		{
			rk4(tar[k][0], xar[k][0], var[k][0], rpunkt, vpunkt, &tar[k][1], &xar[k][1], &var[k][1]);
		}
		
		// Ausgabe der 3 (Mindest)dimensionen
		wegx << tar[0][1] << "\t" << xar[0][1] << "\n";
		wegy << tar[1][1] << "\t" << xar[1][1] << "\n";
		wegz << tar[2][1] << "\t" << xar[2][1] << "\n";
		geschx << tar[0][1] << "\t" << var[0][1] << "\n";
		geschy << tar[1][1] << "\t" << var[1][1] << "\n";
		geschz << tar[2][1] << "\t" << var[2][1] << "\n";

		Ekin = 0; // Reset
		Epot = 0;
		
		for(int k=0; k<d; k++)
		{
			Ekin += 0.5*m*pow(xar[k][1], 2);
			Epot += 0.5*m*pow(var[k][1], 2);
		}
		pot << tar[0][1] << "\t" << Epot << "\n";
		kin << tar[0][1] << "\t" << Ekin << "\n";
		ges << tar[0][1] << "\t" << Epot+Ekin << "\n";
		
		for(int k=0; k<d; k++) // Aus neu macht alt
		{
			tar[k][0] = tar[k][1];
			xar[k][0] = xar[k][1];
			var[k][0] = var[k][1];
			tar[k][1] = 0;
			xar[k][1] = 0;
			var[k][1] = 0;
		}
	}
	return 0;
}
