import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 

# data
s= pd.read_csv('0.csv',error_bad_lines=False)
t= pd.read_csv('1.csv',error_bad_lines=False)
u= pd.read_csv('2.csv',error_bad_lines=False)
v= pd.read_csv('3.csv',error_bad_lines=False)
xdata  = s['a']
ydata= s['b']
x1data  = t['a']
y1data= t['b']
x2data  = u['a']
y2data= u['b']
x3data  = v['a']
y3data= v['b']

# Create plots with pre-defined labels.
# Alternatively, you can pass labels explicitly when calling `legend`.
fig, ax = plt.subplots()
ax.plot(xdata, ydata, 'k--', label='n=0')
ax.plot(x1data, y1data, 'r:', label='n=1')
ax.plot(x2data, y2data, 'k', label='n=2')
ax.plot(x3data, y3data, 'b', label='n=3')


# Now add the legend with some customizations.
legend = ax.legend(loc='upper center', shadow=True)

# The frame is matplotlib.patches.Rectangle instance surrounding the legend.
frame = legend.get_frame()
frame.set_facecolor('0.90')

# Set the fontsize
for label in legend.get_texts():
    label.set_fontsize('large')

for label in legend.get_lines():
    label.set_linewidth(1.5)  # the legend line width
plt.savefig("A2_a.pdf")
plt.show()
