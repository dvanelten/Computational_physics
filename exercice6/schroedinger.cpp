#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>
#include <complex>
#include <stdio.h>


using namespace Eigen;
using namespace std;

typedef complex<double> C;
//Definier delta-Funktion
int delta (const int i, const int j)
{
    return (int) i==j;
}

complex<double> j(0,1);
double EpsilonMin = -2;
double EpsilonMax = 2;
double delEpsilon = 0.1;
//double dim = (EpsilonMax- EpsilonMin)/delEpsilon;
int dim = 201;
double delta_tau = 0.02;
double epsilon0 = 1.;
double sigma = 1;

void initPsi(double delEpsilon, int dim){
	//Psi-Vektor
	VectorXcd psi =  VectorXcd::Zero(dim);
	double epsilon =-10;
	for (double i = 0; i<dim; i++)
	{ 
		double sigma = 1;
		psi(i) = pow(1. / 2. / M_PI / sigma, 0.25)*exp(-(pow(epsilon-epsilon0 ,2)) / 4 / sigma);  //Beschreibt Gauss-Paket
		epsilon += delEpsilon;
	}
	double norm = 0;	
	norm = psi.norm();
	norm = sqrt(norm);
	psi  = psi/norm;
	//cout << psi(2).real() << endl;
	//cout << norm << endl;
	//Plot des Zustandsvektors bei t=0
 	ofstream DataPsi("psi.csv");
 	DataPsi << "a" << "," << "b" <<"\n";
 	for(int i = 0; i <dim ; i++){
 		DataPsi << i << "," << psi(i).real()*psi(i).real() <<"\n";
 	}
 	//Ab hier kommt die Zeitentwicklung in n-Schritten
 	MatrixXcd H = MatrixXcd::Zero(dim,dim);
  	for (int i = 0; i<dim; i++){
		for (int j = 0; j<dim; j++){
			if (i == j) H(i,j) = (2 /(delEpsilon*delEpsilon) + i*i*delEpsilon*delEpsilon);
			if (i == j - 1 || i == j + 1) H(i,j) = (-1 / (delEpsilon*delEpsilon));
			if (i != j && i != j + 1 && i != j - 1) H(i,j) = 0;
		}
	}
	cout << H(0,0).real() << endl;

	MatrixXcd ident = MatrixXcd::Identity(dim,dim);
	MatrixXcd S_H = MatrixXcd::Zero(dim,dim);
	MatrixXcd S_1 = MatrixXcd::Zero(dim,dim);
	MatrixXcd S_2 = MatrixXcd::Zero(dim,dim);
	S_1 = ident + j * H*delta_tau;
	S_2 = S_1.conjugate();
	S_H = S_1.inverse()*S_2;
	//Nun soll der Vektor bei t=10 berechnet werden
	VectorXcd psi_t =  VectorXcd::Zero(dim);
	ofstream Datanorm("norm.csv");
	Datanorm << "a" << "," << "b" << "\n";
 	for(double n = 0; n < 10; n+=delta_tau){
 		psi_t = S_H*psi;
 		Datanorm << n <<","<< psi_t.norm() <<"\n"; 
 		S_H = S_H*S_H;
 	}
 	cout << psi_t << endl;
 	cout << psi_t.norm() << endl;

 	/*//Datenpunkte für den Zustandsvektor bei t=10
 	ofstream DataPsi_10("psi_10.txt");
 	for(int i = 0; i <dim ; i++)
 		DataPsi_10 << i << "	" << psi_t(i)*psi_t(i) <<"\n";
 	//Hier sollen die Datenpunkte der Normierung, als auch der 'Animation' berechnet werden
 	ofstream DataPsi_0("psi_0.txt"); //t=0
 	ofstream DataPsi_2("psi_3.txt"); //t=3
 	ofstream DataPsi_3("psi_3.txt"); //t=3
 	ofstream DataPsi_4("psi_4.txt"); //t=4
 	ofstream DataPsi_5("psi_5.txt"); //t=5
 	ofstream DataPsi_6("psi_6.txt"); //t=6
 	ofstream DataPsi_7("psi_7.txt"); //t=7
 	ofstream DataPsi_8("psi_8.txt"); //t=8
 	ofstream DataPsi_9("psi_9.txt"); //t=9
 	ofstream DataNorm("norm.txt");
 	VectorXcd psi_0 =  VectorXcd::Zero(dim);
 	VectorXcd psi_2 =  VectorXcd::Zero(dim);
 	VectorXcd psi_3 =  VectorXcd::Zero(dim);
 	VectorXcd psi_4 =  VectorXcd::Zero(dim);
 	VectorXcd psi_5 =  VectorXcd::Zero(dim);
 	VectorXcd psi_6 =  VectorXcd::Zero(dim);
 	VectorXcd psi_7 =  VectorXcd::Zero(dim);
 	VectorXcd psi_8 =  VectorXcd::Zero(dim);
 	VectorXcd psi_9 =  VectorXcd::Zero(dim);  */
 	/*for(int j = 0; j< 10; j++)
 		{
		S_H = S_H*S_H;
		//Berechnung der Datenpunkte für Animation
		if(j==0){
			psi_0 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_0 << i << "	" << psi_0(i).real()*psi_0(i).real() <<"\n";
			DataNorm << j <<"	"<< psi_0.norm() <<"\n";}
			
		if(j==2){
			psi_2 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_2 << i << "	" << psi_2(i).real()*psi_2(i).real() <<"\n";
			DataNorm << j <<"	"<< psi_2.norm() <<"\n";}	
			
		if(j==3){
			psi_3 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_3 << i << "	" << psi_3(i).real()*psi_3(i).real() <<"\n"; 	
			DataNorm << j <<"	"<< psi_3.norm() <<"\n";}		
		if(j==4){
			psi_4 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_4 << i << "	" << psi_4(i).real()*psi_4(i).real() <<"\n"; 	
			DataNorm << j <<"	"<< psi_4.norm() <<"\n";}
		if(j==5){
			psi_5 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_5 << i << "	" << psi_5(i).real()*psi_5(i).real() <<"\n"; 	
			DataNorm << j <<"	"<< psi_5.norm() <<"\n";}
		if(j==6){
			psi_6 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_6 << i << "	" << psi_6(i).real()*psi_6(i).real() <<"\n"; 	
			DataNorm << j <<"	"<< psi_6.norm() <<"\n";}		
		if(j==7){
			psi_7 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_7 << i << "	" << psi_7(i).real()*psi_7(i).real() <<"\n"; 			
			DataNorm << j <<"	"<< psi_7.norm() <<"\n";}		
		if(j==8){
			psi_8 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_8 << i << "	" << psi_8(i).real()*psi_8(i).real() <<"\n"; 	
			DataNorm << j <<"	"<< psi_8.norm() <<"\n";}
		if(j==9){
			psi_9 = S_H*psi;
			for(int i = 0; i <dim ; i++)
				DataPsi_9 << i << "	" << psi_9(i).real()*psi_9(i).real() <<"\n"; 
			DataNorm << j <<"	"<< psi_9.norm() <<"\n";}						
 		}*/
 	//Berechnung der Datenpunkte der zur Überprüfung der Normierung
 	/*VectorXcd psi_norm =  VectorXcd::Zero(dim);
 	for(double j = 0; j< 10; j+=delta_tau){
 		psi_norm = S_H*psi_norm;
		//for(int i = 0; i <dim ; i++)
			//DataNorm << i << "	" << psi_norm(i).real()*psi_norm(i).real() <<"\n";
		DataNorm << j <<"	"<< psi_norm.norm() <<"\n";}
 	*/}
	


int main()
{
 double delEpsilon = 1;
 initPsi(delEpsilon,dim); 	//Berechnung des Zustandsvektors bei t= 0
}








