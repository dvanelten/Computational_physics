import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
 
# Messdaten
hista = np.loadtxt("B10A1a.txt", unpack=True)
histb = np.loadtxt("B10A1b.txt", unpack=True)
 
#bin, hist1, hist2, hist3, hist4 = np.loadtxt("B7A1_histo.txt", unpack=True)

plt.clf()
plt.grid()
plt.hist(hista, 25, facecolor='green')
plt.title("Histogramm Zufallsverteilung")
plt.xlabel('phi')
plt.ylabel('$p_{phi}$')
plt.savefig("B10A1a_hist.pdf")

plt.clf()
plt.grid()
plt.hist(histb, 25, facecolor='green')
plt.title("Histogramm Zufallsverteilung")
plt.xlabel('phi')
plt.ylabel('$omega(phi)$')
plt.savefig("B10A1b_hist.pdf")

