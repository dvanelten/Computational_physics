#include <iostream>     // Ein- und Ausgabebibliothek
#include <stdio.h>      // printf
#include <math.h> 		// pow, sin, cos
using namespace std;

double Trapez(double (&f)(double), double a, double b, double h)
{
	double Int=0;
	while(a<b)
		{
		Int += 0.5*h*(f(a)+f(a+h));
		a += h;
		}
	return Int;
}

double Mittel(double (&f)(double), double a, double b, double h)
{
	double Int=0;
	double c=a+h/2;
	b -= c;
	while(c<b)
		{
		Int += h*f(c);
		c += h;
		}
	return Int;
}

double Simpson(double (&f)(double), double a, double b, double h)
{
	if(((int)(((b-a)+1)/h))%2 != 0)
		{
		cout << "Schrittweite nicht möglich."<< endl;
		return 0;
		}
	double Int=0;
	while(a<b)
		{
		Int += h/6*(f(a)+4*f(a+h/2)+f(a+h));
		a += h;
		}
	return Int;
}

double f (double x){
	return exp(-x)/x;
	}

int main(){             // Hauptfunktion
	double a = 1;
	double b = 100;
	double h = 1;
	cout << "Trapez" << Trapez(*f , a, b, h) << endl;	
	cout << "Mittel" << Mittel(*f , a, b, h) << endl;
	cout <<"Simpson" <<Simpson(*f , a, b, h) << endl;
	return 0;
}
	
	