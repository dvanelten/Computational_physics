#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;
int step=1000;
double cw = 0.2, R = 0.11, A = 0.038, m = 0.43, rhoLuft = 1.3, g = 9.81, v_0 = 30, h=0.005;
double fakt = 1/(2*m)*cw*rhoLuft*A;

double rpunkt(double t, double x, double v)
{
    return v;
}

double vpunkt(double t, double x, double v)
{
	return -g-fakt*pow(v,2);
}

void rk4(double t, double x, double v, double rpunkt(double t, double x, double v), double vpunkt(double t, double x, double v), double* tneu, double* xneu, double* vneu)
{
	double k1r, k2r, k3r, k4r, k1v, k2v, k3v, k4v; // k*r und k*v fuer die 4-Schritt-Approximation
	k1r = h*rpunkt(t, x, v); 
	k1v = h*vpunkt(t, x, v);

	k2r = h*rpunkt(t+h/2.0, x+k1r/2.0, v+k1v/2.0);
	k2v = h*vpunkt(t+h/2.0, x+k1r/2.0, v+k1v/2.0);

	k3r = h*rpunkt(t+h/2.0, x+k2r/2.0, v+k2v/2.0);
	k3v = h*vpunkt(t+h/2.0, x+k2r/2.0, v+k2v/2.0);

	k4r = h*rpunkt(t+h, x+k3r, v+k3v);
	k4v = h*vpunkt(t+h, x+k3r, v+k3v);

	*xneu = x + (k1r + 2.0*(k2r+k3r) + k4r)/6.0;
	*vneu = v + (k1v + 2.0*(k2v+k3v) + k4v)/6.0;
	*tneu = t+h;
}

int main(){
	cout.precision(3); 	
	double tar[step];
	double xar[step];
	double var[step];

	ofstream bahn("Bahn.txt");
	bahn << "# win weite" << "\n";
	for(double win = 0; win < 90; win+=90.0/1000.0)
	{
		double alpha = win*M_PI/180;
		//cout << "win: " << win << endl;
		//cout << alpha << endl;
		tar[0] = 0.0;
		xar[0] = R;
		var[0] = v_0*sin(alpha);
		
		for(int t=0; t<step; t++){ 	
			rk4(tar[t], xar[t], var[t], rpunkt, vpunkt, &tar[t+1], &xar[t+1], &var[t+1]); 
			if(xar[t+1] <= 0)
			{
				bahn << win << "\t" << tar[t+1] << "\n";
				t=step;
			}
		}
	}
	return 0;
}

