from math import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants.constants as const

def rkn4(t,x,y,xd,yd,f,h):
    k1x,k1y = f(t,x,y,xd,yd)
    k2x,k2y = f(t+h/2.0,x+h/2.0*xd+h*h/8.0*k1x,y+h/2.0*yd+h*h/8.0*k1y,xd+h/2.0*k1x,yd+h/2.0*k1y)
    k3x,k3y = f(t+h/2.0,x+h/2.0*xd+h*h/8.0*k1x,y+h/2.0*yd+h*h/8.0*k1y,xd+h/2.0*k2x,yd+h/2.0*k2y)
    k4x,k4y = f(t+h,x+h*xd+h*h/2.0*k3x,y+h*yd+h*h/2.0*k3y,xd+h*k3x,yd+h*k3y)
    xnew = x+h*xd+h*h/6.0*(k1x+k2x+k3x)
    ynew = y+h*yd+h*h/6.0*(k1y+k2y+k3y)
    xdnew = xd+h/6.0*(k1x+2.0*k2x+2.0*k3x+k4x)
    ydnew = yd+h/6.0*(k1y+2.0*k2y+2.0*k3y+k4y)
    return (t+h,xnew,ynew,xdnew,ydnew)

def f(t,x,y,vx,vy):
    v = (vx*vx+vy*vy)**0.5
    ablx = -factor*v*vx    # entspricht v**2*(vx/v) 
    ably = -g -factor*v*vy 
    return ablx,ably

v0 = 30
cw = 3 # Widerstandsbeiwert
m = 0.43 # Masse des Korpers
r = 0.11 # ungefaehrer Radius des Koerpers
A = 0.038 # Querschnittsflaeche des Koerpers
rho = 1.3 # Luftdichte
alpha = 70*np.pi/180 # Wurfwinkel alpha
vx = v0*cos(alpha)
vy = v0*sin(alpha)
global g # fuer f verfuegbar machen
g = 9.81 # Fallbeschleunigung
global factor   
factor = cw*rho*A/(2*m)
t = 0.0 # laufende Zeit
x = 0.0
y = float(r) # Startwert x
xd = vx # Startwert xd=v
yd = vy
h = 0.01 # Integration-Zeitschrittweite


while y>=0.0:
    for i in range(1000):
        t0,x0,y0,xd0,yd0 = t,x,y,xd,yd  # alte Werte merken
        t,x,y,xd,yd = rkn4(t,x,y,xd,yd,f,h)
        if y<=0.0: break 
        # t,x,y,xd,yd,(xd**2+yd**2)**0.5
        plt.plot(t, x, "r.")
plt.title("Flugweite")
plt.xlabel('x / m')
plt.ylabel('z / m')
plt.savefig("PlotB3A3neu.pdf")