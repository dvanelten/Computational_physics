/*
 * drehmomente.cpp
 *
 *  Created on: 16.04.2016
 *      Author: Marcel Golz, Fabian Hundehege, Bjoern Wendland
 */
#include<iostream>
#include<complex>
#include<cmath>
#include<cstdlib>
#include<vector>
#include<sstream>
#include<utility>
#include<math.h>
#include<string>
#include<fstream>


using namespace std;


extern const double mu4pi=0.0000001;	//mu durch 4 pi als Konstante
extern const double a=0.01;				//Gitterabstand 0.01 (1cm)

double E1(int N, double theta){
	double E=0;							//zu Beginn ist die Energie 0
	for(int i=-N; i<=N;i++){
		for(int j=-N;j<=N;j++){
			if(i==0 && j==0){			//keine Selbstwechselwirkung
				continue;
			}
			else{
				E += mu4pi/(pow((i*i+j*j) ,1.5) * pow(a,3))*((-3)*(pow((i*i+j*j),-1))*(j*j*cos(theta)+i*j*sin(theta))+cos(theta));
			} //Formel vorher analytisch vereinfacht
		}
	}

	return E;
}

double E2(int N, double theta){  //Analog aber mit Fallunterscheidung
	double E=0;
	for(int i=-N; i<=N;i++){
			for(int j=-N;j<=N;j++){
				if(i==0 && j==0){
					continue;
				}
				else if(abs(i+j)%2){ // Jeder zweite Spin zeigt runter
					E += mu4pi/(pow((i*i+j*j) ,1.5) * pow(a,3))*(3*(pow((i*i+j*j),-1))*(j*j*cos(theta)+i*j*sin(theta))-cos(theta));}
				else{
					E += mu4pi/(pow((i*i+j*j) ,1.5) * pow(a,3))*((-3)*(pow((i*i+j*j),-1))*(j*j*cos(theta)+i*j*sin(theta))+cos(theta));
				}
			}
		}

	return E;
}

double T1(int N, double theta){ //Ableitung mit E1 (symmetrischer Differenzenquotient)
	 double h=0.00001;
	return abs((E1(N,(theta+h))-E1(N,(theta-h)))/(2*h));
}

double T2(int N, double theta){ //Ableitung mit E2 (symmetrischer Differenzenquotient)
	 double h=0.00001;
	return abs((E2(N,(theta+h))-E2(N,theta-h))/(2*h));
}


int main(){


	//txt N=2
	ofstream daten1;
	daten1.open("1.txt");
	daten1<< "x E1 E2"<<endl;
	//******

	//txt  N=5
	ofstream daten2;
	daten2.open("2.txt");
	daten2<< "x E1 E2"<<endl;
		//******

	//txt N=10
	ofstream daten3;
	daten3.open("3.txt");
	daten3<< "x E1 E2"<<endl;
		//******

	//txt N=2
	ofstream daten4;
	daten4.open("4.txt");
	daten4<< "x T1 T2 T3 T4"<<endl; //T1 ableitung von E1, T2 Ableitung von E2, T3 Kreuzprodukt von E1, T4 Kreuzprodunkt von E2
	//******

	//txt N=5
	ofstream daten5;
	daten5.open("5.txt");
	daten5<< "x T1 T2 T3 T4"<<endl;
	//******

	//txt N=10
	ofstream daten6;
	daten6.open("6.txt");
	daten6<< "x T1 T2 T3 T4"<<endl;
	//******

	double h=0.001;
	for(unsigned int i=0;i<6300;i++){
		daten1<< (i*h) << " " << E1(2,(i*h))<<" "<<E2(2,(i*h))<<endl;
		daten2<< (i*h) << " " << E1(5,(i*h))<<" "<<E2(5,(i*h))<<endl;
		daten3<< (i*h) << " " << E1(10,(i*h))<<" "<<E2(10,(i*h))<<endl;
		daten4<< (i*h) << " " << T1(2,(i*h))<< " "<<T2(2,(i*h)) <<" "<< abs(E1(2,(i*h))*tan((i*h)))<< " " << abs(E2(2,(i*h))*tan((i*h)))<< endl;
		daten5<< (i*h) << " " << T1(5,(i*h))<< " "<< T2(5,(i*h))<< " "<< abs(E1(5,(i*h))*tan((i*h))) << " " << abs(E2(5,(i*h))*tan((i*h)))<<endl;
		daten6<< (i*h) << " " << T1(10,(i*h))<< " "<< T2(10,(i*h))<<" "<< abs(E1(10,(i*h))*tan((i*h))) << " " << abs(E2(10,(i*h))*tan((i*h)))<< endl;
	}

	//Die Ableitung ist jeweils identisch mit dem analytischen Ausdruck E*tan(theta)
	//Herleitung:	E=-m*B*cos(theta) (Skalarprodukt)
	//				T=m*B*sin(theta) (Kreuzprodukt)
	//				B=E/(-m*cos(theta))
	//		dann:	T=-E*tan(theta) 		(Nur Betrag betrachtet)

	daten6.close();
	daten5.close();
	daten4.close();
	daten3.close();
	daten2.close();
	daten1.close();
	return 0;
}
