
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>
#include <vector>
#include <stdio.h>
#include <ctime>


using namespace std;
//Definition der logistischen Abbildung laut Aufgabenstellung
double f(double x,double r){return r*x*(1-x);}
//Hier wird ein Teil der Fixpunktgleichung (0.5 fehlt hier) iteriert
double F(int n, double r,double x){
	double a=x;
	int t=pow(2,n);
	for(int i=0;i<t;i++){
		a=f(a,r);
		//cout << a << endl;
	}
	return a;
}
//Mithilfe der Intervallhalbierung werden die Nullstellen R_n für n=1,2,3 bestimmt
double interhalb(double a, double b, int n, double e, double x){
	double c=(a+b)/2.;
	double fc=0.5-F(n,c,x);
	double fa=0.5-F(n,a,x);
	double fb=0.5-F(n,b,x);
	if(abs(fc)<e)return c;
	if(fc*fa<0)return interhalb(a,c,n,e,x);
	else{return interhalb(b,c,n,e,x);}
	return -1.;

}

int main(){
//Aufgabenteil a)
double r_max = 3.5699;
ofstream data0("0.csv");
data0<<"a"<<","<<"b"<<"\n";
ofstream data1("1.csv");
data1<<"a"<<","<<"b"<<"\n";
ofstream data2("2.csv");
data2<<"a"<<","<<"b"<<"\n";
ofstream data3("3.csv");
data3<<"a"<<","<<"b"<<"\n";

for(double r=0;r<r_max;r+=0.0001){
	data0<<r<<","<<0.5-F(0,r,0.5)<<"\n";
	data1<<r<<","<<0.5-F(1,r,0.5)<<"\n";
	data2<<r<<","<<0.5-F(2,r,0.5)<<"\n";
	data3<<r<<","<<0.5-F(3,r,0.5)<<"\n";
}

//Aufgabenteil b)
double e=0.00000001;
double R0, R1, R2, R3;
R0 = interhalb(1.9999,2.0001,0,e,0.5);
R1 = interhalb(3.2360,3.2362,1,e,0.5);
R2 = interhalb(3.4985,3.4986,2,e,0.5);
R3 = interhalb(3.5546,3.5547,3,e,0.5);
cout<<"R0:   "<<R0<<endl;
cout<<"R1:   "<<R1<<endl;
cout<<"R2:   "<<R2<<endl;
cout<<"R3:   "<<R3<<endl;

//Aufgabenteil c)
double feigenbaumkonstante;
feigenbaumkonstante = (R2 - R1)/(R3 - R2);
cout<<"Feigenbaumkonstante:   "<<feigenbaumkonstante<<endl;
cin.get();
return 0;

}

