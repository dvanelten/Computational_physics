#include<iostream>
#include<fstream>
#include<cmath>

using namespace std;
double pi = 3.14159265358979323846;

double seed();
double zufall_gleichverteilt(double min, double max);

int main(){
	// Aufgabenteil a) 
	cout << "Ubung 10 - Aufgabe 2" << endl;
	cout << "Das Integral aus Aufgabenteil a)/b) ergibt: " << endl;
	cout << "N  -  pi  -  rel. Abweichung" << endl;
	
	ofstream b("B10A2b.txt");
	b << "# N pi fehler" << "\n";
	
	ofstream b2("B10A2b2.txt");
	b2 << "# pi fehler" << "\n";
	
	int N, count;
	double xi, yi, pi_wert, fehler;
	for(int k=1; k<=7; k++){
		N = pow(10,k);
		count = 0;
		
		if(N == 1000){
			for(int j=0; j<N; j++){
				for(int i=0; i<N; i+=1){
					xi = zufall_gleichverteilt(0, 1);
					yi = zufall_gleichverteilt(0, 1);
					if(((xi*xi)+(yi*yi)) < 1) count++;
					pi_wert = 4*(double)count/(double)i;
				}
				fehler = (pi_wert-pi)/pi;
				b2 << pi_wert << "\t" << fehler << "\n";
			}
			count = 0;
		}
		
		for(int i=0; i<N; i+=1){
			xi = zufall_gleichverteilt(0, 1);
			yi = zufall_gleichverteilt(0, 1);
			if(((xi*xi)+(yi*yi)) < 1) count++;
			pi_wert = 4*(double)count/(double)i;
		}
		fehler = (pi_wert-pi)/pi;
		if(N==7) cout << "a) ";
		cout << N << " - " << pi_wert << " - " << fehler << endl;
		b << N << "\t" << pi_wert << "\t" << fehler << "\n";
	}
	cout << endl;
	
	// Aufgabenteil c)
	
		
	/*
	ofstream data("B9A1.txt");

	MatrixXd A(N_matrix,N_matrix);
	
	int werte[16] = {1, -2, 2, 4, -2, 3, -1, 0, 2, -1, 6, 3, 4, 0, 3, 5};
	int count = 0;
	
	for(int i=0; i<N_matrix; i++){
		for(int j=0; j<N_matrix; j++){
			A(i,j) = werte[count];
			count++;
		}
	}

	// Aufgabenteil a) EW �ber Eigen
	EigenSolver<MatrixXd> es(A); // Computes eigenvalues and eigenvectors of general matrices
	data << "a) EW mit Eigen: " << "\n" << es.eigenvalues() << "\n" << "\n";

	// Aufgabenteil b) Potenzmethode
	double EW;
	data << "b) EW mit Potenzmethode: " << "\n"; 
	
	for(int i=0; i<N_matrix; i++){
		potenzmethode(A,EW);
		data << "(" << EW << ", 0)" << "\n";
	} */
	
	
	
	return 0;
}

double seed(){
	return (double)rand()/(double)RAND_MAX;
}

double zufall_gleichverteilt(double min, double max){
	return ((double)rand()/((double)RAND_MAX+1)*(max-min))+min;
}


