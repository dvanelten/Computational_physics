#include<iostream>
#include<fstream>
#include<cmath>
#include<Eigen/Dense>

using Eigen::MatrixXd;
using Eigen::EigenSolver;
using Eigen::VectorXd;
using namespace std;

int anzahl_iteration = 100;
int N_matrix = 4;
void potenzmethode(MatrixXd &A, double &EWmax);

int main(){
	ofstream data("B9A1.txt");

	MatrixXd A(N_matrix,N_matrix);
	
	int werte[16] = {1, -2, 2, 4, -2, 3, -1, 0, 2, -1, 6, 3, 4, 0, 3, 5};
	int count = 0;
	
	for(int i=0; i<N_matrix; i++){
		for(int j=0; j<N_matrix; j++){
			A(i,j) = werte[count];
			count++;
		}
	}

	// Aufgabenteil a) EW �ber Eigen
	EigenSolver<MatrixXd> es(A); // Computes eigenvalues and eigenvectors of general matrices
	data << "a) EW mit Eigen: " << "\n" << es.eigenvalues() << "\n" << "\n";

	// Aufgabenteil b) Potenzmethode
	double EW;
	data << "b) EW mit Potenzmethode: " << "\n"; 
	
	for(int i=0; i<N_matrix; i++){
		potenzmethode(A,EW);
		data << "(" << EW << ", 0)" << "\n";
	}
	return 0;
}

void potenzmethode(MatrixXd &A, double &EWmax){
	VectorXd v_n(N_matrix); 
	v_n.setZero();
	v_n(0) = 1; // = 1; 0; 0; 0; Startvektor
	VectorXd w_n(N_matrix);
	
	// Potenzmethode: Iteration
	for(int i=0; i<anzahl_iteration; i++){
		w_n = A*v_n; // Iteration
		v_n = w_n/w_n.norm(); // Normierung
	}
	
	// Maximaler Eigenwert:
	EWmax = v_n.transpose()*A*v_n; // Diagonalisierung um aus EV den EW zu bestimmen
	
	// Ver�ndern der Matrix, damit maximaler EW 0 entspricht und neuer EWmax vorhanden ist
	MatrixXd B(N_matrix,N_matrix);
	for(int i=0; i<N_matrix; i++){ // Berechnet x_1 x x_1
		for(int j=0; j<N_matrix; j++){
			B(i,j) = v_n(j)*v_n(i);
		}
	}
	A = A-EWmax*B; // Skript: S.152 
}
