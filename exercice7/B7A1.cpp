#include<iostream>
#include<fstream>
#include<cmath>
#include <array>

using namespace std;

// unsigned long long -> 64 Bit int
unsigned long long generator(unsigned long long rn, unsigned long long a, unsigned long long c, unsigned long long m){
	return (a*rn+c)%m;
}

int main(){
	ofstream data("B7A1.txt");
	data << "# para1 para2 para3 para4" << "\n";
	ofstream korr("B7A1_korr.txt");
	korr << "# x1 y1 x2 y2 x3 y3 x4 y4" << "\n";
	
	int N = 100000; // N < m 
	int count = 0;

	// Aufgabenteil b
	int const para_anz = 4;
	std::array<unsigned long long, para_anz> r = {1234, 1234, 123456789, 1234};
	std::array<unsigned long long, para_anz> a = {20, 137, 65539, 16807};
	std::array<unsigned long long, para_anz> c = {120, 187, 0, 0};
	std::array<unsigned long long, para_anz> m = {6075, 256, 2147483648, 2147483647};
	
	double zuf;
		
	for(int i=0; i<N-1; i++){ // Anzahl Zufallszahlen
		data << i << "\t";
		count += 1;
		for(int num=0; num<para_anz; num++){ // fuer die 4 Parametersaetze
			// Aufgabenteil c: count
			N = 100000; // Zur�cksetzen
			
			// Aufgabenteil a: floating-point-Gen f�r [0,1[ 
			r[num] = generator(r[num],a[num],c[num],m[num]);
			zuf = double(r[num])/double(m[num]);
			data << zuf << "\t";
			
			// Aufgabenteil c: N < m check:
			if(N>=m[num]){
				N = m[num]-1; 
			} 
			if(N>count){
				korr << zuf << "\t"; // x-Wert
			}else{
				korr << 0 << "\t";
			}
				
			// Aufgabenteil c: Naechste Zufallszahl
			r[num] = generator(r[num],a[num],c[num],m[num]);
			zuf = double(r[num])/double(m[num]);
			
			// Aufgabenteil c: y-Wert speichern
			if(N>count){
				korr << zuf << "\t"; // y-Wert
			}else{
				korr << 0 << "\t";
			}
		}
		data << "\n";
		korr << "\n";
		N = 100000; // Zur�cksetzen
	}
	return 0;
}
