#include<iostream>
#include<fstream>
#include<cmath>

using namespace std;
double pi = 3.14159265358979323846;
int delta = 1;

double zufall_gleichverteilt(double min, double max);
double kugelflache_gleichverteilt();
double omega(double theta);
double phi_next(double phi, double alpha);

int main(){
	
	// Aufgabenteil a)
	ofstream a("B10A1a.txt");
	a << "# theta" << "\n";
	int N = 10000;
	double hilf, theta;
	
	// Aufgabenteil b)
	ofstream b("B10A1b.txt");
	b << "# omega_theta" << "\n";
	double omega_theta=0, sum_b=0;
	
	// Aufgabenteil c)
	double theta_start[N];
	
	// Aufgabenteil a)
	for(int i=0; i<N; i+=0){
		hilf = zufall_gleichverteilt(0,pi);
		theta = zufall_gleichverteilt(0,pi); 
		if((0.5*sin(theta)) > hilf){
			a << theta << "\n";
			i++;
				
			// Aufgabenteil b)
			omega_theta = omega(theta);
			b << omega_theta << "\n";
			sum_b += omega_theta;
			
			// Aufgabenteil c)
			theta_start[i-1] = theta;
		}
	}
	
	// Aufgabenteil b)
	cout << "Aufgabenteil b): Numerisch ermittelte Mittelwert betraegt: " << endl;
	cout << sum_b/N << endl;
	
	// Aufgabenteil c)
	double tau = 0.75;
	double alpha = 10*pi/180;

	for(int tp = 0.1; tp < 100; tp += 0.1){
		
	}
	
	
		
	/*
	ofstream data("B9A1.txt");

	MatrixXd A(N_matrix,N_matrix);
	
	int werte[16] = {1, -2, 2, 4, -2, 3, -1, 0, 2, -1, 6, 3, 4, 0, 3, 5};
	int count = 0;
	
	for(int i=0; i<N_matrix; i++){
		for(int j=0; j<N_matrix; j++){
			A(i,j) = werte[count];
			count++;
		}
	}

	// Aufgabenteil a) EW �ber Eigen
	EigenSolver<MatrixXd> es(A); // Computes eigenvalues and eigenvectors of general matrices
	data << "a) EW mit Eigen: " << "\n" << es.eigenvalues() << "\n" << "\n";

	// Aufgabenteil b) Potenzmethode
	double EW;
	data << "b) EW mit Potenzmethode: " << "\n"; 
	
	for(int i=0; i<N_matrix; i++){
		potenzmethode(A,EW);
		data << "(" << EW << ", 0)" << "\n";
	} */
	
	
	
	return 0;
}

double zufall_gleichverteilt(double min, double max){
	return ((double)rand()/(double)RAND_MAX*(max-min))+min;
}

double kugelflache_gleichverteilt(){
	double g, h;
	do{ 
		g = zufall_gleichverteilt(0,pi);
		h = zufall_gleichverteilt(0,pi); 
	}while((0.5*sin(h)) > g);
	return h;
}

double omega(double theta){
	return 0.5*delta*(3*cos(theta)*cos(theta)-1);
}

double phi_next(double phi, double alpha){
	return acos(cos(alpha)*cos(phi)-sin(alpha)*sin(phi)*cos(zufall_gleichverteilt(0, 2*pi)));
}




