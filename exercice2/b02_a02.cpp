#include <iostream>
#include <string>
#include <fstream>
#include "math.h"
#include "stdlib.h"
using namespace std;

double a = 1.0;
double rho_0 = 1.0;
double eps_0 = 8.854187817*(pow(10,-12));
double h = 0.01;

double mittelpunkt_3D(double (&integrand)(double, double, double, double), double val, double lowLimX, double upLimX, double lowLimY, double upLimY, double lowLimZ, double upLimZ, double width){
	double output = 0.0; 
	
	double x_k = lowLimX + width/2; 
	double y_k = lowLimY + width/2; 
	double z_k = lowLimZ + width/2; 
	while(x_k <= upLimX-width/2){
		while(y_k <= upLimY-width/2){ 
			while(z_k <= upLimZ-width/2){
				output = output + width * integrand(val, x_k, y_k, z_k);
				z_k = z_k + width;
			}
			z_k = lowLimZ + width/2;
			y_k = y_k + width;
		}
		y_k = lowLimY + width/2;
		x_k = x_k + width;
	}
	
	return output;
}

double func_a(double x, double x_, double y_, double z_){
	return 1 / (pow((x - x_)*(x - x_) + y_*y_ + z_*z_,0.5));
}

double func_c(double x, double x_, double y_, double z_){
	return (x_/a) / (pow((x - x_)*(x - x_) + y_*y_ + z_*z_,0.5));
}

double phi(double x, double (&func)(double, double, double, double)){
	return mittelpunkt_3D(*func, x, -a, a, -a, a, -a, a, h) * rho_0 / (4 * M_PI * eps_0);
}

void printer(char *name, unsigned int minVal, unsigned int maxVal, double (&func)(double, double, double, double)){
	// Erstellen / Oeffnen der Dateien
	ofstream outputDatei;	
	outputDatei.open(name);
	if (!outputDatei.is_open()) {
		cerr << "Datei konnte zum Schreiben nicht geoeffnet werden!\n";
		exit(1);
	}
	// Erste Zeile der Tabelle als Uebersicht schreiben
	outputDatei << "x \t phi";
	
	double x = 0.0;
	for (unsigned int n = minVal; n <= maxVal; n++){
		x = 0.1 * n * a;
		outputDatei << "\n" << x << "\t" << phi(x, *func); 
	}
	outputDatei.close();
}

int main() {
	// Dateinamen fuer die Ergebnisse 
	char datei_a[] = "b02_a02_a.txt";
	char datei_b[] = "b02_a02_b.txt";
	char datei_c[] = "b02_a02_c.txt";
	// Berechnung der Ergebnisse 
	printer(datei_a, 11, 80, *func_a);
	printer(datei_b, 0, 10, *func_a);
	printer(datei_c, 0, 80, *func_c);
	
	return 0;
}
