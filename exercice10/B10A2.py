import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
 
# Messdaten
N, pi, fehler = np.loadtxt("B10A2b.txt", unpack=True)
#histb = np.loadtxt("B10A1b.txt", unpack=True)

fig = plt.figure()

ax1 = fig.add_subplot(211)
ax1.plot(N, pi, "rx")
plt.title("Pi-Werte mit N Zufallszahlen")
plt.xlabel('N')
plt.ylabel('pi')

ax2 = fig.add_subplot(212)
ax2.plot(N, fehler, "rx")
plt.title("Fehler der ermittelten Pi-Werte")
plt.xlabel('N')
plt.ylabel('\epsilon')

ax.set_xscale('log')
ax.set_yscale('log')

plt.tight_layout()
plt.savefig("PlotB10A2_pi.pdf")