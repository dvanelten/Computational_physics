import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
 
# Messdaten
xweg, Wegx = np.loadtxt("Wegx.txt", unpack=True)
yweg, Wegy = np.loadtxt("Wegy.txt", unpack=True)
zweg, Wegz = np.loadtxt("Wegz.txt", unpack=True)
xgesch, Geschx = np.loadtxt("Geschwindigkeitx.txt", unpack=True)
ygesch, Geschy = np.loadtxt("Geschwindigkeity.txt", unpack=True)
zgesch, Geschz = np.loadtxt("Geschwindigkeitz.txt", unpack=True)
tpot, Pot = np.loadtxt("PotEnergie.txt", unpack=True)
tkin, Kin = np.loadtxt("KinEnergie.txt", unpack=True)
tges, Ges = np.loadtxt("GesEnergie.txt", unpack=True)
 
fig = plt.figure()

ax1 = fig.add_subplot(331)
ax1.plot(xweg, Wegx, "r.")
plt.title("Weg in x-Richtung")
plt.xlabel('t / s')
plt.ylabel('x / m')

ax2 = fig.add_subplot(332)
ax2.plot(yweg, Wegy, "r.")
plt.title("Weg in y-Richtung")
plt.xlabel('t / s')
plt.ylabel('y / m')

ax3 = fig.add_subplot(333)
ax3.plot(zweg, Wegz, "r.")
plt.title("Weg in z-Richtung")
plt.xlabel('t / s')
plt.ylabel('z / m')

ax4 = fig.add_subplot(334)
ax4.plot(xgesch, Geschx, "r.")
plt.title("Geschw. x-Richtung")
plt.xlabel('t / s')
plt.ylabel('$v_x$ / m/s')

ax5 = fig.add_subplot(335)
ax5.plot(ygesch, Geschy, "r.")
plt.title("Geschw. y-Richtung")
plt.xlabel('t / s')
plt.ylabel('$v_y$ / m/s')

ax6 = fig.add_subplot(336)
ax6.plot(zgesch, Geschz, "r.")
plt.title("Geschw. z-Richtung")
plt.xlabel('t / s')
plt.ylabel('$v_z$ / m/s')

ax7 = fig.add_subplot(337)
ax7.plot(tpot, Pot, "r.")
plt.title("Pot. Energie")
plt.xlabel('t / s')
plt.ylabel('E / J')

ax8 = fig.add_subplot(338)
ax8.plot(tkin, Kin, "r.")
plt.title("Kin. Energie")
plt.xlabel('t / s')
plt.ylabel('E / J')

ax9 = fig.add_subplot(339)
ax9.plot(tges, Ges, "r.")
plt.title("Gesamtenergie")
plt.xlabel('t / s')
plt.ylabel('E / J')

plt.tight_layout()
plt.savefig("PlotB3A1.pdf")