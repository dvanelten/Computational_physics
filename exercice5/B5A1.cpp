#include <iostream>
#include <fstream>
#include<stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>

using namespace std;

int N=16, L=8, m=1, Nbins=100; // Teilchenanzahl, Boxl�nge und Masse
double h=0.01, tstop=1;

double ZweiDLJ(double r)
{
	return 4*(pow((1/r),12)-pow((1/r),6));
}

double ZweiDLJAbl(double r)
{
	return 4*(-12*pow(r,-13)+6*pow(r,-7));
}

void initialisieren(vector<vector <double> > &rvec, vector<vector <double> > &vvec, double T);
void beschleunigung(vector<vector <double> > &xvec, vector<vector <double> > &avec);
double vschwerpunkt(vector<vector <double> > &vvec);
double Ekin(vector<vector<double> > &vvec);
double Epot(vector<vector<double> > &xvec);
double temperatur(vector<vector<double> > &vvec);
void paarkorr(vector<vector<double> > &rvec, vector<double> &paar, double T, double bins);


int main(){
	vector<vector<double> > v1, v2, r1, r2, a1, a2;
	double T = 1;
	
	// Aufgabenteil a)	
	initialisieren(r1, v1, T); // r1 und v1 werden festgelegt 
	beschleunigung(r1, a1); // a1 wird hier erst beschrieben
	
	ofstream data("B5A1.txt");
	data << "# t rx ry Epot Ekin Eges T vSpx vSpy Korr" << "\n";
	
	// Paarkorrelation
	vector<double> paar;
	int bins = 100;
	int mitt = pow(10,4);
	double anzahl = tstop/h;
	double step = (L/2.0)/bins;
	ofstream paartxt("B5A1_paar.txt");
	paartxt << "# r g" << "\n"; 
	
	for(double t=h; t<=tstop; t+=h){
		for(int i=0; i<N; i++){ // Verlet-Algorithmus
			vector<double> s;
			s.push_back(r1[i][0] + v1[i][0] * h + 0.5*a1[i][0] * h*h); 
			s.push_back(r1[i][1] + v1[i][1] * h + 0.5*a1[i][1] * h*h);
			
			// Wenn aus der Box rausfliegt, muss es periodisch reinfliegen S.75 (1)
			if (s[0]>L){ s[0] -= L; }
			if (s[1]>L){ s[1] -= L; }
			if (s[0]<0){ s[0] += L; }
			if (s[1]<0){ s[1] += L; }
			r2.push_back(s);
		}

		//neue Geschwindigkeiten berechnen (mit Verlet-Algrotimus)
		beschleunigung(r2, a2);
		for(int i=0; i<N; i++){
			vector<double> s;
			s.push_back(v1[i][0] + 0.5*(a2[i][0] + a1[i][0])*h);
			s.push_back(v1[i][1] + 0.5*(a2[i][1] + a1[i][1])*h);
			v2.push_back(s);
		}
		data << t << "\t" << Epot(r2) << "\t" << Ekin(v2) << "\t" << Ekin(v2) + Epot(r2) << "\t" << temperatur(v2) << "\t" << vschwerpunkt(v2) << "\n";
		
		// Paarkoor
		//paarkorr(r1, paar, T, bins); // Funktioniert leider nicht
		if(t=tstop){
			int count = 0;
			for(int r=0; r<=(L/2); r+=step){
				//paartxt << r << "\t" << paar[count] << "\n"; 
				count++;	
			}
		}
		
		// Reset
		r1 = r2;
		v1 = v2;
		a1 = a2;
		a2.clear();
		v2.clear();
		r2.clear();
	}
	return 0;
}

void initialisieren(vector<vector<double> > &rvec, vector<vector<double> > &vvec, double T)
{
	vector<double> ges_v(2, 0);
	
	//Anfangsort und -geschwindigkeit setzen
	for (int i = 0; i<4; i++){
		for (int j = 0; j<4; j++){
			vector<double> r;
			r.push_back((1 + 2 * i)*L / 8);
			r.push_back((1 + 2 * j)*L / 8);
			vector<double> v;
			v.push_back(pow((double)rand() / (double)RAND_MAX - 0.5, 1)); // Zwischen -0.5 und 0.5
			v.push_back(pow((double)rand() / (double)RAND_MAX - 0.5, 1));

			ges_v[0] += v[0];
			ges_v[1] += v[1];

			rvec.push_back(r);
			vvec.push_back(v);
		}
	}
	
	//Summe �ber alle Anfangsgeschwindigkeiten sollte 0 sein
	double xanpassen = ges_v[0] / N;
	double yanpassen = ges_v[1] / N;
	for(int i=0; i<vvec.size(); i++){
		vvec[i][0] -= xanpassen;
		vvec[i][1] -= yanpassen;
	}

	//Summe �ber alle quadrierten Geschwindigkeiten bestimmen
	double vbetrag = 0.0;
	for(int i=0; i<vvec.size(); i++)
	{
		vbetrag += sqrt(pow(vvec[i][0],2) + pow(vvec[i][1],2));
	}
	
	//Umskalierung f�r unterschiedliche Temperaturen
	double Tist = m*pow(vbetrag,2)/N; // Formel: 2/(kBN)*sum^N_i pi^2/(2m) S.70
	double skal = T/Tist; // T = gewuenschte Temp und Tist instantane Temp
	for (int i=0; i<vvec.size(); i++)
	{
		vvec[i][0] *= skal;
		vvec[i][1] *= skal;
	}
}

void beschleunigung(vector<vector <double> > &xvec, vector<vector <double> >&avec){
	for(int i=0; i<xvec.size(); i++){ // Aktuelles Teilchen
		vector<double> c(2, 0.0);
		for(int j=0; j<xvec.size(); j++){ // Anderes Teilchen
			if (i != j){ // Unterschiedliche Teilchen (5.7)
				for(int n=-1; n<=1; n++){ // Spiegelk�sten links rechts oben unten werden betrachtet S.76
					for(int m=-1; m<=1; m++){ // (5.7)
						double rx = xvec[i][0]-xvec[j][0]+n*L; // (5.7)
						double ry = xvec[i][1]-xvec[j][1]+m*L;
						double rbet = sqrt(pow(rx,2)+pow(ry,2));
						if (rbet < (L/2.0)){
							if(ry==0){ // Falls eine Richtung ausgeschlossen werden kann, sonst nan
								c[0] -= rx/abs(rx)*ZweiDLJAbl(abs(rx));
							}
							if(rx==0){
								c[1] -= ry/abs(ry)*ZweiDLJAbl(abs(ry));
							}
							// cout << "c: " << c[0] << ", " << c[1] << endl;
						}
					}
				}
			}
		}
		//cout << i << ", c0: " << c[0] << ", c1: " << c[1] << endl;
		avec.push_back(c);
	}
}

double vschwerpunkt(vector<vector<double> > &vvec){
	double vx = 0, vy = 0;
	for(int i = 0; i<vvec.size(); i++){
		vx += vvec[i][0];
		vy += vvec[i][1];
	}
	vx = vx/vvec.size();
	vy = vy/vvec.size();
	return sqrt(pow(vy,2)+pow(vx,2));
}

double temperatur(vector<vector<double> > &vvec){
	return 2*Ekin(vvec)/N; // Formel S. 77 T=2*E_kin/(kb N_f)
}

double Ekin(vector<vector<double> > &vvec){
	double sumEkin = 0.0;
	for(int i=0; i<vvec.size(); i++){
		sumEkin += 0.5*m*(pow(sqrt(pow(vvec[i][0],2) + pow(vvec[i][1],2)),2));
	}
	return sumEkin;
}

double Epot(vector<vector<double> > &rvec){
	double epot = 0;
	for(int i=0; i<rvec.size(); i++){
		for(int j=0; j<rvec.size(); j++){
			if(i<j-1){ // �bungsblatt + S.83
				for(int n=-1; n<=1; n++){ // Randbedingungen nicht vergessen
					for(int m=-1; m<=1; m++){
						double rbet = sqrt(pow((rvec[i][0]-rvec[j][0]+n*L),2) + pow((rvec[i][1]-rvec[j][1]+m*L),2));
						if(rbet < (L/2)){
							epot += 0.5*ZweiDLJ(rbet);
						}
					}
				}
			}
		}
	}
	return epot;
}

void paarkorr(vector<vector<double> > &rvec, vector<double> &paar, double T, double bins){
	double step = (L/2.0)/bins; // Schrittweite r
	int anzahl, count = 0;
	double teilchendichte = N/pow(L,2); 
	double paarkorr;
	
	for(int r=0; r<=(L/2); r+=step){
		count++;
		anzahl = 0;
		for(int i=0; i<rvec.size(); i++){
			for(int j=0; j<rvec.size(); j++){
				for(int n=-1; n<=1; n++){ // Randbedingungen nicht vergessen
					for(int m=-1; m<=1; m++){
						double rbet = sqrt(pow((rvec[i][0]-rvec[j][0]+n*L),2) + pow((rvec[i][1]-rvec[j][1]+m*L),2));
						if(rbet <= r && rbet < (L/2)){
							anzahl++;
						}
					}
				}
			}
		}
		// Formel berechnen und speichern
		paarkorr = anzahl/N/teilchendichte;
		paar[count] += paarkorr;
	}
}

