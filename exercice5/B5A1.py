import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress

# Messdaten
t, Epot, Ekin, Eges, T, vsp = np.loadtxt("B5A1.txt", unpack=True)

plt.clf()
plt.grid()
plt.plot(t, Ekin, "r-", label="Ekin")
plt.plot(t, Epot, "g-", label="Epot")
plt.plot(t, Eges, "b-", label="Eges")
plt.xlabel('t / s')
plt.ylabel('Energie / J')
plt.legend(loc="best")
plt.savefig("B5A1_Energie.pdf")

plt.clf()
plt.grid()
plt.plot(t, T, "r-", label="Temperatur")
plt.xlabel('t / s')
plt.ylabel('Temperatur / K')
plt.legend(loc="best")
plt.savefig("B5A1_Temp.pdf")

plt.clf()
plt.grid()
plt.plot(t, vsp, "r-", label="v im Schwerpunkt")
plt.xlabel('t / s')
plt.ylabel('Geschwindigkeit / m/s')
plt.legend(loc="best")
plt.savefig("B5A1_Geschw.pdf")