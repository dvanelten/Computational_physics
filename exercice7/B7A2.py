import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
import matplotlib.mlab as mlab
import math
 
# Messdaten
zuf = np.loadtxt("B7A2_Boxmuller.txt", unpack=True)
y = np.loadtxt("B7A2_Grenzwertsatz.txt", unpack=True)
p1 = np.loadtxt("B7A2_Rueckweisungsverfahren.txt", unpack=True)
p2 = np.loadtxt("B7A2_Transformationsmethode.txt", unpack=True)

plt.clf()
plt.grid()
result = plt.hist(zuf, 30, facecolor='green')
plt.title("Parameterset 4")
plt.xlabel('x')
plt.ylabel('Häufigkeit')
plt.xlim((min(zuf), max(zuf)))

mu = 0
variance = 1
sigma = math.sqrt(variance)
x = np.linspace(min(zuf), max(zuf),100)
plt.plot(x, mlab.normpdf(x, mu, sigma)*31500, 'r-')
plt.savefig("B7A2_Boxmuller.pdf")

plt.clf()
plt.grid()
result = plt.hist(y, 30, facecolor='green')
plt.title("Parameterset 4")
plt.xlabel('x')
plt.ylabel('Häufigkeit')
plt.xlim((min(y), max(y)))

x = np.linspace(min(y), max(y),100)
plt.plot(x, mlab.normpdf(x, mu, sigma)*28500, 'r-')
plt.savefig("B7A2_Grenzwertsatz.pdf")


plt.clf()
plt.grid()
result = plt.hist(p1, 30, facecolor='green')
plt.title("Parameterset 4")
plt.xlabel('x')
plt.ylabel('Häufigkeit')
plt.xlim((0, np.pi))
x = np.linspace(0, np.pi,100)
plt.plot(x, (np.sin(x)/2)*10500, 'r-')
plt.savefig("B7A2_Rueckweisungsverfahren.pdf")


plt.clf()
plt.grid()
result = plt.hist(p2, 30, facecolor='green')
plt.title("Parameterset 4")
plt.xlabel('x')
plt.ylabel('Häufigkeit')
plt.xlim((min(p2), max(p2)))
x = np.linspace(min(p2), max(p2),100)
plt.plot(x, 3*x**2*3300, 'r-')
plt.savefig("B7A2_Transformationsmethode.pdf")