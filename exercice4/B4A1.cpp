#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;
double h = 0.0001, g = 9.81, L = 1.0, lambda = L/L, m = 1.0, mu = m/(m+m);

double phipunkt1(double t, double phi1, double phip1) 
{
    return phip1;
}

double phipunkt2(double t, double phi2, double phip2) 
{
    return phip2;
}

double phipunktpunkt1(double t, double phi1, double phi2, double phip1, double phip2)
{
    return (1/(1-mu*pow(cos(phi2-phi1),2)))*(mu*g*sin(phi2)*cos(phi2-phi1)+mu*pow(phip1,2)*sin(phi2-phi1)*cos(phi2-phi1)-g*sin(phi1)+(mu/lambda)*pow(phip2,2)*sin(phi2-phi1));
}

double phipunktpunkt2(double t, double phi1, double phi2, double phip1, double phip2)
{
    return (1/(1-mu*pow(cos(phi2-phi1),2)))*(g*sin(phi1)*cos(phi2-phi1)-mu*pow(phip2,2)*sin(phi2-phi1)*cos(phi2-phi1)-g*sin(phi2)-lambda*pow(phip1,2)*sin(phi2-phi1));
}

double eukl2(double x, double xs)
{
	return sqrt(pow(x-xs,2));
}

double eukl4(double x, double xs, double y, double ys)
{
	return sqrt(pow(x-xs,2)+pow(y-ys,2));
}

void rk4(double t, double phi1, double phi2, double phip1, double phip2, double phipunkt1(double t, double phi1, double phip1), double phipunkt2(double t, double phi2, double phip2), double phipunktpunkt1(double t, double phi1, double phi2, double phip1, double phip2), double phipunktpunkt2(double t, double phi1, double phi2, double phip1, double phip2), double* tneu, double* phi1neu, double* phi2neu, double* phip1neu, double* phip2neu)
{
	double k1phi1 = h*phipunkt1(t, phi1, phip1); 
	double k1phi2 = h*phipunkt2(t, phi2, phip2); 
	double k1phip1 = h*phipunktpunkt1(t, phi1, phi2, phip1, phip2); 
	double k1phip2 = h*phipunktpunkt2(t, phi1, phi2, phip1, phip2); 
		
	double k2phi1 = h*phipunkt1(t+h/2.0, phi1+k1phi1/2.0, phip1+k1phip1/2.0);
	double k2phi2 = h*phipunkt2(t+h/2.0, phi2+k1phi2/2.0, phip2+k1phip2/2.0);	
	double k2phip1 = h*phipunktpunkt1(t+h/2.0, phi1+k1phi1/2.0, phi2+k1phi2/2.0, phip1+k1phip1/2.0, phip2+k1phip2/2.0);
	double k2phip2 = h*phipunktpunkt2(t+h/2.0, phi1+k1phi1/2.0, phi2+k1phi2/2.0, phip1+k1phip1/2.0, phip2+k1phip2/2.0);
		
	double k3phi1 = h*phipunkt1(t+h/2.0, phi1+k2phi1/2.0, phip1+k2phip1/2.0);
	double k3phi2 = h*phipunkt2(t+h/2.0, phi2+k2phi2/2.0, phip2+k2phip2/2.0);
	double k3phip1 = h*phipunktpunkt1(t+h/2.0, phi1+k2phi1/2.0, phi2+k2phi2/2.0, phip1+k2phip1/2.0, phip2+k2phip2/2.0);
	double k3phip2 = h*phipunktpunkt2(t+h/2.0, phi1+k2phi1/2.0, phi2+k2phi2/2.0, phip1+k2phip1/2.0, phip2+k2phip2/2.0);
	
	double k4phi1 = h*phipunkt1(t+h, phi1+k3phi1, phip1+k3phip1);
	double k4phi2 = h*phipunkt2(t+h, phi2+k3phi2, phip2+k3phip2);
	double k4phip1 = h*phipunktpunkt1(t+h, phi1+k3phi1, phi2+k3phi2, phip1+k3phip1, phip2+k3phip2);
	double k4phip2 = h*phipunktpunkt2(t+h, phi1+k3phi1, phi2+k3phi2, phip1+k3phip1, phip2+k3phip2);	
	
	*tneu = t+h;
	*phi1neu = phi1 + (k1phi1 + 2.0*(k2phi1+k3phi1) + k4phi1)/6.0;
	*phi2neu = phi2 + (k1phi2 + 2.0*(k2phi2+k3phi2) + k4phi2)/6.0;
	*phip1neu = phip1 + (k1phip1 + 2.0*(k2phip1+k3phip1) + k4phip1)/6.0;
	*phip2neu = phip2 + (k1phip2 + 2.0*(k2phip2+k3phip2) + k4phip2)/6.0;
}


int main(){
	ofstream doppendel("doppendel.txt"); 
	ofstream traj("trajektorie.txt");
	cout << "Es wird das Doppelpendel betrachtet und " << endl;
	cout << "mit Hilfe des Runge-Kutta-Verfahrens 4. Ordnung geloest." << endl;

	double tar[2], phi1ar[2], phi2ar[2], phip1ar[2], phip2ar[2];
	double Ekin, Epot, Eges, x2Bew, y2Bew;

	// Anfangsbedingungen
	tar[0] = 0;
	phi1ar[0] = 0;
	phi2ar[0] = 0;
	phip1ar[0] = 0;
	phip2ar[0] = 4.472;
	
	// Ekin und Epot f�r Aufgabe 1 c)
	//Ekin = (m/2)*pow(L,2)*(2*pow(phip1ar[0],2)+pow(phip2ar[0],2)+2*phip1ar[0]*phip2ar[0]*cos(phi1ar[0]-phi2ar[0]));
	Ekin = (m/2)*(pow(L*phip1ar[0]*cos(phi1ar[0]),2)+pow(L*phip1ar[0]*sin(phi1ar[0]),2)) + (m/2)*(pow(L*phip1ar[0]*cos(phi1ar[0])+L*phip2ar[0]*cos(phi2ar[0]),2)+pow(L*phip1ar[0]*sin(phi1ar[0])+L*phip2ar[0]*sin(phi2ar[0]),2));
	// Epot = m*g*L*(2*cos(phi1ar[0])+cos(phi2ar[0]));
	Epot = -m*g*(2*L*cos(phi1ar[0])+L*cos(phi2ar[0]));
	Eges = Ekin + Epot; // Definitionssache
	doppendel << "# t phi1 phi2 phip1 phip2 Ekin Epot Eges" << "\n";
	doppendel << tar[0] << "\t" << 	phi1ar[0] << "\t" << phi2ar[0] << "\t" << phip1ar[0] << "\t" << phip2ar[0] << "\t" << Ekin << "\t" << Epot << "\t" << Eges << "\n";
	
	// Trajektorien f�r Aufgabe 1 d)
	x2Bew = L*sin(phi1ar[0]) + L*sin(phi2ar[0]);
	y2Bew = -L*cos(phi1ar[0]) - L*cos(phi2ar[0]);
	traj << "# t x2 y2" << "\n";
	traj << tar[0] << "\t" << x2Bew << "\t" << y2Bew << "\n"; 
	
	// Aufgabenteil 2 b)
	int s;
	cout << "Wird eine Stoerung (2b)) mit betrachtet? Ja=1, Nein=0" << endl;
	cout << "Antwort: ";
	cin >> s;
	
	double phi1sar[2], phi2sar[2], phip1sar[2], phip2sar[2], stor, abstand1, abstand2;
	ofstream storung("storung.txt");
	storung << "# t phi1 abs1 phi2 abs2" << "\n";
	if(s==1)
	{
		stor = 0.0;
		phi1sar[0] = phi1ar[0] + stor;
		phi2sar[0] = phi2ar[0] + stor;
		phip1sar[0] = phip1ar[0] + stor;
		phip2sar[0] = phip2ar[0] + stor;
		abstand1 = eukl4(phi1ar[0], phi1sar[0], phip1ar[0], phip1sar[0]);
		abstand2 = eukl4(phi2ar[0], phi2sar[0], phip2ar[0], phip2sar[0]);
		storung << tar[0] << "\t" << phi1ar[0] << "\t" << abstand1 << "\t" << phi2ar[0] << "\t" << abstand2 << "\n";
	}
	
	for(double t=0; t<10; t+=h){
		rk4(tar[0], phi1ar[0], phi2ar[0], phip1ar[0], phip2ar[0], phipunkt1, phipunkt2, phipunktpunkt1, phipunktpunkt2, &tar[1], &phi1ar[1], &phi2ar[1], &phip1ar[1], &phip2ar[1]);
	
		Ekin = 0;
		Epot = 0;
		Eges = 0;
		
		// Aufgabe 1 c)
		Ekin = (m/2)*(pow(L*phip1ar[1]*cos(phi1ar[1]),2)+pow(L*phip1ar[1]*sin(phi1ar[1]),2)) + (m/2)*(pow(L*phip1ar[1]*cos(phi1ar[1])+L*phip2ar[1]*cos(phi2ar[1]),2)+pow(L*phip1ar[1]*sin(phi1ar[1])+L*phip2ar[1]*sin(phi2ar[1]),2));
		Epot = -m*g*(2*L*cos(phi1ar[1])+L*cos(phi2ar[1]));
		//Ekin = (m/2)*pow(L,2)*(2*pow(phip1ar[1],2)+pow(phip2ar[1],2)+2*phip1ar[1]*phip2ar[1]*cos(phi1ar[1]-phi2ar[1]));
		//Epot = m*g*L*(2*cos(phi1ar[1])+cos(phi2ar[1]));
		Eges = Ekin + Epot; // Definitionssache
		doppendel << tar[1] << "\t" << 	phi1ar[1] << "\t" << phi2ar[1] << "\t" << phip1ar[1] << "\t" << phip2ar[1] << "\t" << Ekin << "\t" << Epot << "\t" << Eges << "\n";
	
		// Aufgabe 1 d)
		x2Bew = L*sin(phi1ar[1]) + L*sin(phi2ar[1]);
		y2Bew = -L*cos(phi1ar[1]) - L*cos(phi2ar[1]);
		traj << tar[1] << "\t" << x2Bew << "\t" << y2Bew << "\n"; 
	
		// Aufgabe 2 b)
		if(s==1)
		{	
			rk4(tar[0], phi1sar[0], phi2sar[0], phip1sar[0], phip2sar[0], phipunkt1, phipunkt2, phipunktpunkt1, phipunktpunkt2, &tar[1], &phi1sar[1], &phi2sar[1], &phip1sar[1], &phip2sar[1]);
			
			abstand1 = 0;
			abstand2 = 0;
			abstand1 = eukl4(phi1ar[0], phi1sar[0], phip1ar[0], phip1sar[0]);
			abstand2 = eukl4(phi2ar[0], phi2sar[0], phip2ar[0], phip2sar[0]);
			storung << tar[0] << "\t" << phi1ar[0] << "\t" << abstand1 << "\t" << phi2ar[0] << "\t" << abstand2 << "\n";
			
			phi1sar[0] = phi1sar[1];
			phi1sar[1] = 0;
			phi2sar[0] = phi2sar[1];
			phi2sar[1] = 0;
			phip1sar[0] = phip1sar[1];
			phip1sar[1] = 0;
			phip2sar[0] = phip2sar[1];
			phip2sar[1] = 0;	
		}
	
		// Aus neu macht alt
		tar[0] = tar[1];
		tar[1] = 0; 
		phi1ar[0] = phi1ar[1];
		phi1ar[1] = 0;
		phi2ar[0] = phi2ar[1];
		phi2ar[1] = 0;
		phip1ar[0] = phip1ar[1];
		phip1ar[1] = 0;
		phip2ar[0] = phip2ar[1];
		phip2ar[1] = 0;
		
	}
	return 0;
}
