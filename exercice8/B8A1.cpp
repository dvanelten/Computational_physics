#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<vector>
#include<stdio.h>
#include<ctime>

using namespace std;
double const cut_max = 0.001; // 0.1

double funk_1(double xn, double r) // Logistisch
{
	return r*xn*(1.0-xn);
}

double funk_2(double xn, double r)
{
	return r*xn - pow(xn, 3); // Kubisch
}

void bifurkation(int funk, double x, double r, vector<double> &xn, int N_x);

int main(){
	
	srand(time(NULL)); 
	double r_max = 5;
	double N_x = 100;
	
	ofstream log("B8A1_log.txt");
	log << "# t f \n";
	ofstream kub("B8A1_kub.txt");
	kub << "# t f \n";
	
	vector<double> vek_1, vek_2;
	double x_1 = 0, x_2 = 0;
	
	for(double r = 0.001; r < r_max; r += 0.001){  
		double random=(double)rand()/(double)RAND_MAX; // Startwert zwischen 0 und 1
		bifurkation(1, random, r, vek_1, N_x);
		for(int i = 0; i < vek_1.size(); i++){
			if(x_1 != vek_1[i]) log << r << "\t" << vek_1[i] << "\n"; // Reduziert gleiche Werte
			x_1 = vek_1[i];
		}
		
		bifurkation(2, random, r, vek_2, N_x);
		for(int i = 0; i < vek_2.size(); i++){
			if(x_2 != vek_2[i]) kub << r << "\t" << vek_2[i] << "\n";
			x_2 = vek_2[i];
		}
		
		vek_1.clear();
		vek_2.clear();
	}
	return 0;
}


void bifurkation(int funk, double x, double r, vector<double> &xn, int N_x){	
	vector<double> hilfsvek_x;
	int count, count_werte, count_fehler;
	double abs;
	
	hilfsvek_x.push_back(x); // Startx in Vektor einf�gen
	count = 1; 
	for(int i = 0; i <= N_x; i++){ // F�r alle alten xn (hilfsvek_x) den Abstand zum neuen x berechnen
		if(funk == 1) x = funk_1(x, r);
		if(funk == 2) x = funk_2(x, r);
		
		for(int j = 0; j <= count-1; j++)
		{
			abs = sqrt(pow(x-hilfsvek_x[j], 2));
			if(abs < cut_max){ 
				xn.push_back(x); 
				count_werte++;
				if(count_werte >= N_x) i=N_x; 
			}else{
				i--; // Noch kein neuer Wert ermittelt
				count_fehler++;
				if(count_fehler >= 10000) i=N_x; // Abbrechbed.
			}
		}
		hilfsvek_x.push_back(x); // Neuer Wert zu alten hinzuf�gen
		count++; // Z�hlt die alten xn
	}
}
