import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
 
# Messdaten
x_kub, y_kub = np.loadtxt("B8A1_kub.txt", unpack=True)
x_log, y_log = np.loadtxt("B8A1_log.txt", unpack=True)

plt.grid()
plt.plot(x_kub, y_kub, "r.") #, ms=1)
plt.title("Kubische Abbildung")
plt.xlabel('$r_{kub}$ / m')
plt.ylabel('$x_{kub}$ / m')
plt.savefig("B8A1_kub.pdf")

plt.clf()
plt.grid()
plt.plot(x_log, y_log, "r.") #, ms=1)
plt.title("Logistische Abbildung")
plt.xlabel('$r_{log}$ / m')
plt.ylabel('$x_{log}$ / m')
plt.savefig("B8A1_log.pdf")