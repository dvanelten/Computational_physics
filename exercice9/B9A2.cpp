#include<iostream>
#include<fstream>
#include<complex>
#include<math.h>
#include<Eigen/Dense>

using Eigen::MatrixXd;
using Eigen::MatrixXcd;
using Eigen::SelfAdjointEigenSolver;
using Eigen::VectorXd;

using namespace std;

int N_EW = 10;

int dimension(int L, double delta_epsilon);
void hamilton(MatrixXd &H, double delta_epsi, int dim, double lambda);
double delta(int i, int j);
void anharmonizitat_symmetrie(MatrixXd &H, int dimension);
void anharmonizitat(MatrixXd &H, int dimension, double lambda);

void hamiltonStoerung(MatrixXd &S, double dim, double lambda);

int main(){
	double lambda = 0.0;
	double lambda_tilde = 0.2;
	int L = 10;
	double delta_epsilon = 0.1;
	int dim = dimension(L, delta_epsilon);
	
	ofstream data("B9A2.txt");
	data.precision(4);
	MatrixXd Hb(dim,dim);
	
	// Aufgabenteil b: Kontrolle mit lambda
	hamilton(Hb, delta_epsilon, dim, lambda);
	SelfAdjointEigenSolver<MatrixXd> es(Hb);
	VectorXd EW = es.eigenvalues();
	
	data << "2a) EW n�herungsweise mit lambda = " << lambda << " f�r dim = " << dim << "\n";
	int n;
	for(int i=0; i<N_EW; i++){
		n = EW(i)/2 - (1/2);
		data << EW(i) << " f�r n = " << n << "\n";
	}	
	
	// Aufgabenteil b: lambda_tilde
	hamilton(Hb, delta_epsilon, dim, lambda_tilde);
	SelfAdjointEigenSolver<MatrixXd> es_tilde(Hb);
	EW = es_tilde.eigenvalues();
	
	data << "\n" << "\n" << "2b) EW n�herungsweise mit lambda = " << lambda_tilde << " und dim = " << dim << "\n";
	for(int i=0; i<N_EW; i++){
		data << EW(i) << ", ";
	}
	
	// Aufgabenteil d: Diskretisierung ver�ndern
	double delta_epsilon_array[] = {1, 0.5, 0.2, 0.05, 0};
	for(int i=0; i<4; i++){
		dim = dimension(L, delta_epsilon_array[i]);
		data << "\n" << "\n" << "2d) EW mit lambda = " << lambda_tilde << ", Delta epsilon = " << delta_epsilon_array[i] << " und dim = " << dim << "\n";
		if(i==0){
			MatrixXd H0(dim,dim);
			hamilton(H0, delta_epsilon_array[i], dim, lambda_tilde);
			SelfAdjointEigenSolver<MatrixXd> es_H0(H0);
			EW = es_H0.eigenvalues();
		}
		if(i==1){
			MatrixXd H1(dim,dim);
			hamilton(H1, delta_epsilon_array[i], dim, lambda_tilde);
			SelfAdjointEigenSolver<MatrixXd> es_H1(H1);
			EW = es_H1.eigenvalues();
		}
		if(i==2){
			MatrixXd H2(dim,dim);
			hamilton(H2, delta_epsilon_array[i], dim, lambda_tilde);
			SelfAdjointEigenSolver<MatrixXd> es_H2(H2);
			EW = es_H2.eigenvalues();	
		}
		if(i==3){
			MatrixXd H3(dim,dim);
			hamilton(H3, delta_epsilon_array[i], dim, lambda_tilde);
			SelfAdjointEigenSolver<MatrixXd> es_H3(H3);
			EW = es_H3.eigenvalues();
		}
		for(int j=0; j<N_EW; j++){
			data << EW(j) << ", ";
		} 
	}	
	
	// Aufgabenteil c: Symmetrie �berpr�fen
	data << "\n" << "\n" << "\n" << "2c) Symmetrie �berpr�fen" << "\n";
	data.precision(4);
	dim = 10;
	MatrixXd Hc(dim,dim);
	anharmonizitat_symmetrie(Hc, dim);
	for(int n=0; n<dim; n++){
		for(int m=0; m<dim; m++){
			data << Hc(n,m) << "\t";
		}
		data << "\n";
	}
	
	// Aufgabenteil c: Hamiltonian mit St�rung
	dim = 50;
	MatrixXd Hcs(dim,dim);
	data << "\n" << "\n" << "\n" << "2c) EW mit lambda = " << lambda_tilde << " und dim = " << dim << "\n";
	anharmonizitat(Hcs, dim, lambda_tilde);
	SelfAdjointEigenSolver<MatrixXd> es_anharm(Hcs);
	EW = es_anharm.eigenvalues();	
	for(int i=0; i<N_EW; i++){
		data << EW(i) << ", ";
	}
	
	// Aufgabenteil d: Hamiltonian mit St�rung f�r verschiedene dim
	int dim_array[] = {10, 20, 30, 100, 0};
	for(int i=0; i<4; i++){
		data << "\n" << "\n" << "2d) EW mit lambda = " << lambda_tilde << " und dim = " << dim_array[i] << "\n";
		dim = dim_array[i];
		if(i==0){
			MatrixXd H0(dim,dim);
			anharmonizitat(H0, dim, lambda_tilde);
			SelfAdjointEigenSolver<MatrixXd> es_dim0(H0);
			EW = es_dim0.eigenvalues();	
		}
		if(i==1){
			MatrixXd H1(dim,dim);
			anharmonizitat(H1, dim, lambda_tilde);
			SelfAdjointEigenSolver<MatrixXd> es_dim1(H1);
			EW = es_dim1.eigenvalues();	
		}
		if(i==2){
			MatrixXd H2(dim,dim);
			anharmonizitat(H2, dim, lambda_tilde);
			SelfAdjointEigenSolver<MatrixXd> es_dim2(H2);
			EW = es_dim2.eigenvalues();	
		}
		if(i==3){
			MatrixXd H3(dim,dim);
			anharmonizitat(H3, dim, lambda_tilde);
			SelfAdjointEigenSolver<MatrixXd> es_dim3(H3);
			EW = es_dim3.eigenvalues();	
		}
		for(int j=0; j<N_EW; j++){
			data << EW(j) << ", ";
		} 
	}
	
	

	/*

	
	int dim2 = 50;
	int dim3 = 201;
	int dim4 = 100;
	
	double delEpsilon2 = 0.4; //geh�rt zu dim 50
	double delEpsilon3 = 0.0995;// dim 201
	double delEpsilon4 = 0.2; //dim 100
	
	MatrixXd H2(dim3, dim3);
	MatrixXd H3(dim4,dim4);
	MatrixXd HB(dim2, dim2);
	MatrixXd HB2(dim3, dim3);
	//b
	
	data << "\n" << "In Besetzungszahldarstellung: " << "\n";
	//c
	//ohne St�rung
	HamiltonStoerung(HB, dim2, 0);
	SelfAdjointEigenSolver<MatrixXd> es3(HB);
	VectorXd Eigenwert4 = es3.eigenvalues();
	data << "\n" << "\n" << "2c) Eigenwerte ohne St�rung und dim " << dim2;
	for (int i = 0; i < 10; i++){
		data << "\n" << Eigenwert4(i);
	}

	//mit St�rung
	HamiltonStoerung(HB, dim2, 0.2);
	SelfAdjointEigenSolver<MatrixXd> es4(HB);
	Eigenwert4 = es4.eigenvalues();
	data << "\n" << "\n" << "2c) Eigenwerte mit St�rung und dimension " << dim2;
	for (int i = 0; i < 10; i++){
		data << "\n" << Eigenwert4(i);
	}

	//mit St�rung und dim 100
	HamiltonStoerung(HB2, dim3, 0.2);
	SelfAdjointEigenSolver<MatrixXd> es5(HB2);
	VectorXd Eigenwert2 = es5.eigenvalues();
	data << "\n" << "\n" << "2c) Eigenwerte mit St�rung und dim " << dim3;
	for (int i = 0; i < 10; i++){
		data << "\n" << Eigenwert2(i);
	}

	*/
	
	return 0;
}

int dimension(int L, double delta_epsilon){
	return 2*L/delta_epsilon + 1;
}

void hamilton(MatrixXd &H, double delta_epsi, int dim, double lambda){
	for(int i=0; i<dim; i++){
		for(int j=0; j<dim; j++){
			double n = (i-(dim-1)/2); // Von -100 bis 100 
			if(i==j) H(i,j) = 2/(delta_epsi*delta_epsi) + delta_epsi*delta_epsi*n*n + lambda*pow(delta_epsi,4)*pow(n,4);
			if(i==j-1 || i==j+1) H(i,j) = -1/(delta_epsi*delta_epsi);
			if(i!=j && i!=j+1 && i!=j-1) H(i,j) = 0;
		}
	}
}

void anharmonizitat_symmetrie(MatrixXd &H, int dimension){
	for(int n=0; n<dimension; n++){
		for(int m=0; m<dimension; m++){
			if(n==m-4) H(n,m) = 0.25*sqrt(m*(m-1)*(m-2)*(m-3)); 
			if(n==m+4) H(n,m) = 0.25*sqrt((m+1)*(m+2)*(m+3)*(m+4));
			if(n==m-2) H(n,m) = 0.25*(sqrt(m*(m-1))*(4*m-2));
			if(n==m+2) H(n,m) = 0.25*(sqrt((m+1)*(m+2))*(4*m+6));
			if(n==m) H(n,m) = 0.25*(6*m*m + 6*m + 3);
			if(n!=m-4 && n!=m+4 && n!=m-2 && n!=m+2 && n!=m) H(n,m) = 0.00;
		}
	}
}

void anharmonizitat(MatrixXd &H, int dimension, double lambda){
	for(int n=0; n<dimension; n++){
		for(int m=0; m<dimension; m++){
			if(n==m-4) H(n,m) = 0.25*lambda*sqrt(m*(m-1)*(m-2)*(m-3)); 
			if(n==m+4) H(n,m) = 0.25*lambda*sqrt((m+1)*(m+2)*(m+3)*(m+4));
			if(n==m-2) H(n,m) = 0.25*lambda*(sqrt(m*(m-1))*(4*m-2));
			if(n==m+2) H(n,m) = 0.25*lambda*(sqrt((m+1)*(m+2))*(4*m+6));
			if(n==m) H(n,m) = 2*(n+0.5)+0.25*lambda*(6*m*m + 6*m + 3);
			if(n!=m-4 && n!=m+4 && n!=m-2 && n!=m+2 && n!=m) H(n,m) = 0.00;
		}
	}
}
