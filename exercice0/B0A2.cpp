#include <iostream>
#include <math.h>
#include <fstream>
#include <functional>
using namespace std;

double f1(double x);
double f1_neu(double x);
double f2(double x);
double f2_neu(double x);
double f3(double x,double delta);
double f3_neu(double x,double delta);

int main(){

	fstream ausgabe1;
	fstream ausgabe2;
	fstream ausgabe3;

	double start1 = 1.0E7;
	double ende1 = 1.0E17;


	ausgabe1.open("Funktion1.txt",ios::out);
	for (double x = start1; x <= ende1; x*=10){
		double rel_fehler1 = (f1(x)-f1_neu(x))/f1(x);
		ausgabe1 << x << "\t" << f1(x) << "\t" << f1_neu(x) << "\t" << rel_fehler1 << endl;
	}


	double start2 = 1.0E-7;
	double ende2 = 1.0E-17;
	
	ausgabe2.open("Funktion2.txt",ios::out);
	for (double x = start2; x >= ende2; x*=0.1){
		double rel_fehler2 = (f2(x)-f2_neu(x))/f2(x);
		ausgabe2 << x << "\t" << f2(x) << "\t" << f2_neu(x) << "\t" << rel_fehler2 << endl;
	}
	
	int x = 3;
	ausgabe3.open("Funktion3.txt",ios::out);
	for (double delta = start2; delta >= ende2; delta*=0.1){
		double rel_fehler3 = (f3(x,delta)-f3_neu(x,delta))/f3(x,delta);
		ausgabe3 << delta << "\t" << f3(x,delta) << "\t" << f3_neu(x,delta) << "\t" << rel_fehler3 << endl;
	}
}

//Funktion 1

double f1(double x)
	{
		return (1/sqrt(x))-(1/sqrt(x+1));
	}
	
double f1_neu(double x)
	{
		return 1/(sqrt(x*(x+1))*(sqrt(x+1)+sqrt(x)));
	}

double rel_fehler1(double f1, double f1_neu, double x)
	{
		return (f1-f1_neu)/f1;
	}

//Funktion 2
	
double f2(double x)
	{
		return (1-cos(x))/sin(x);
	}
	
double f2_neu(double x)
	{
		return sin(x)/(1+cos(x));
	}	

double rel_fehler2(double f2, double f2_neu, double x)
	{
		return (f2-f2_neu)/f2;
	}
//Funktion 3

double f3(double x,double delta)
	{
		return sin(x+delta)-sin(x);
	}
	
double f3_neu(double x,double delta)
	{
		return -2*sin(x)*sin(delta/2)*sin(delta/2)+cos(x)*sin(delta);
	}	
	
double rel_fehler3(double f3, double f3_neu, double x)
	{
		return (f3-f3_neu)/f3;
	}	
