#include<iostream>
#include<fstream>
#include<cmath>
#include<math.h>
#include<array>

using namespace std;
const double M_PI = 3.14159265358979323846;

// unsigned long long -> 64 Bit int
unsigned long long generator(unsigned long long rn, unsigned long long a, unsigned long long m){
	return (a*rn)%m;
}

double boxmuller_1(double u1, double u2){
	return sqrt(-2*log(u1))*cos(2*M_PI*u2);
}

double boxmuller_2(double u1, double u2){
	return sqrt(-2*log(u1))*sin(2*M_PI*u2);
}

double grenzwertsatz(double xi[], const int sumpara){
	double xi_sum;
	for(int i=0; i<sumpara; i++){
		xi_sum += xi[i];
	}
	return xi_sum - double(sumpara)/2.0;
}

double rueck (double zufall){
	return sin(zufall)/2.0;
	}

double trafo (double r){
	return pow(r,1.0/3.0);
}

int main(){
	ofstream data("B7A2_Boxmuller.txt");
	data << "# z" << "\n";
		
	int N = 100000; // N < m 
	unsigned long long r = 1234;
	unsigned long long a = 16807;
	unsigned long long m = 2147483647;
	
	//Aufgabenteil a)
	double u1, u2;
	for(int i=0; i<N/2.0; i++){ // Anzahl Zufallszahlen, pro Schritt werden 2 nach der Verteilung erzeugt
		u1 = double(r)/double(m);
		r = generator(r,a,m);
		u2 = double(r)/double(m);
		data << boxmuller_1(u1,u2) << "\n";
		data << boxmuller_2(u1,u2) << "\n"; 
	}	
		
	//Aufgabenteil b)	
	ofstream data_b("B7A2_Grenzwertsatz.txt");
	data_b << "# y" << "\n";
	int sumpara = 12;	
	double summanden[sumpara];
	double y;

	for(int i=0; i<N;i++){			
		for(int j=0; j<sumpara; j++){
			r = generator(r,a,m);
			summanden[j] = double(r)/double(m);
		}
		y = grenzwertsatz(summanden, sumpara);
		data_b << y << "\n";
	}
	
	//Aufgabenteil c)
	ofstream data_c("B7A2_Rueckweisungsverfahren.txt");
	data_c << "# b" << "\n";
	double g,h;
	for(int i=0; i<N; i+=0){ // Anzahl Zufallszahlen
		h = double(r)/double(m)*M_PI;
		r = generator(r,a,m);
		g = double(r)/double(m)*M_PI;
		if(rueck(h)>g){
			data_c << h << "\n"; 
			i++;
		}	
	}
	

	//Aufgabenteil d)
	ofstream data_d("B7A2_Transformationsmethode.txt");
	data_d << "# p2" << "\n";
	
	for(int i=0; i<N;i++){
		r = generator(r,a,m);
		data_d << trafo(double(r)/double(m)) << "\n";
	}
	
	return 0;
}
