#include <iostream>
#include <cmath>

using namespace std;

double funktiona(double t);
double funktionb1(double t);
double funktionb2(double t);
double mittelpunkt(double f(double x),double a, double b, int n);
double mittelpunkthauptwert(double f(double x),double a, double b, double e, int n);
double anzahlstutzstelle(double f1(double x), double a, double b, double vergl, double e);

int main(){
	double a1=-1, b1=1, a2=0, b2=100000, c=1; // b2 ist eine N�herung f�r inf
	int n1=1000, n21=1000000000, n22; // n1 f�r Funktion 1, n21 f�r die inf N�herung und n22 als ideale Anzahl f�r Funktion 2
	double e=pow(10, -5), fb, fanal= sqrt(M_PI); // fb speichert Funktion b, fanal ist der analytisch berechnete Wert von b)
	
	cout.precision(9); 	
	cout << "a) Integral der Funktion 1 (f(t) = exp(t)/t) mit a=" << a1 << ", b=" << b1 << endl << endl;
	cout << "1) Einfacher Weg: Stuetzstellen so waehlen, dass 0 kein Stuetzpunkt ist, " << endl; 
        cout << "denn dieser ist problematisch (gerades n) mit n=" << n1 << " betraegt:  I=" << mittelpunkt(funktiona,a1,b1,n1) << endl;
        cout << "Der Fehler zum Kontrollwert liegt bei " << abs(mittelpunkt(funktiona,a1,b1,n1)-2.1145018)/2.1145018*100 << "%" << endl << endl;
        cout << "2) Ueber das Hauptwertintegral. Pro Groessenordnung werden n=" << n1 << endl;
        cout << "Stuetzpunkte erzeugt. Das Integral betraegt: I=" << mittelpunkthauptwert(funktiona,a1,b1,0,n1) << endl;
        cout << "Der Fehler zum Kontrollwert liegt bei " << abs(mittelpunkthauptwert(funktiona,a1,b1,0,n1)-2.1145018)/2.1145018*100 << "%" << endl << endl << endl << endl;
	
        cout << "b) Integral der Funktion 2 (f(t) = exp(-t)/sqrt(t)) wird vereinfacht mit a=" << a2 << "," << endl;
        cout << "infty als b=" << b2 << " und n=" << n21 << " berechnet." << endl;
        cout << "Das Integral betraegt:  I=" << mittelpunkt(funktionb1,a2,b2,n21) << endl;
        cout << "Der rel. Fehler zum Kontrollwert liegt bei " << abs(mittelpunkt(funktionb1,a2,b2,n21)-1.77245385)/1.77245385*100 << "%." << endl <<endl;
        cout << "Mittels Substitution wird die Integralgrenze infty veraendert und neu berechnet." << endl;
        n22 = anzahlstutzstelle(funktionb2, a2, c, fanal, e);
        cout << "Analytisch ergibt das Integral einen Wert von: I=" << fanal << endl;
        cout << "Mit gegebenem rel. Fehler von e=" << e << "sollten insgesamt" << endl;
        cout << "mindestens " << n22 << " Stuetzstellen vorhanden sein." << endl;
	fb = mittelpunkt(funktionb2,a2,c,1200000000);
	cout << "Das Integral betraegt: I=" << fb << endl;
	cout << "Der rel. Fehler zum analytischen Wert liegt bei " << abs(fb-fanal)/fanal*100 << "%." << endl <<endl;
	return 0;
}

double funktiona(double t) 
{
	return exp(t)/t;
}

double funktionb1(double t)
{
	return exp(-t)/sqrt(t);
}

double funktionb2(double t)
{
	return exp(-t)/sqrt(t)+(exp(-1/t)/sqrt(1/t))/pow(t,2); // Ergibt sich aus der Substitution
}

double mittelpunkt(double f(double x),double a, double b, int n)
{
	double h=(b-a)/n;
	double sum=0;
	for(int i=1; i<n; i++)
	{
		sum+=f(a-h/2+i*h);
	}
	return h*sum;
}

double mittelpunkthauptwert(double f(double x),double a, double b, double e, int n)
{ // Funktion welche den Wert (= Polstelle) e von oben und unten n�hert, dabei pro Groessenordnung n Stuetzstellen hat
	double hplus, hminus, bneu, aneu;
	double sumplus=0, summinus=0;
	for(double eplus = e+(b*0.1); eplus >= e+1e-6; eplus*=(b*0.1))
	{ // Groessenordnungen durchlaufen f�r den oberen (=plus) Bereich und Integral berechnen
		bneu = eplus/(b*0.1); 
		hplus = abs((bneu-eplus)/n); 
		for(int i=1; i<=n; i++)
		{
			sumplus += hplus*f(bneu+hplus/2-i*hplus); // Hier wird das Integral berechnet
		}
	}
	
	for(double eminus = e+(a*0.1); eminus <= e-1e-6; eminus*=(abs(a)*0.1))
	{ // Analog: Integral berechnen f�r den Bereich unterhalb der Polstelle
		aneu = eminus/(abs(a)*0.1);
		hminus = (aneu-eminus)/n; 
		for(int i=1; i<=n; i++)
		{
			summinus += abs(hminus)*f(aneu+hminus/2-i*hminus);
		}
	}
	return summinus+sumplus; // Zusammenaddieren
}

double anzahlstutzstelle(double f1(double x), double a, double b, double vergl, double e)
{
	cout << "Dieser Prozess dauert etwas laenger." << endl;
	int n = 100000; 
	double h=(b-a)/n;
	double sum= 5; // F�r den ersten Schleifendurchlauf, damit die Bed. des rel. Fehlers erf�llt ist, Wert ausgedacht
	while(abs((sum-vergl)/vergl) > e) 
	{ // Je nach Gr��e des rel Fehlers wird die Schrittweite ver�ndert. Hier Werte ausgew�hlt, die zur Funktion passen, 
		if(abs((sum-vergl)/vergl) < 2*e){ // welche untersucht wird, damit die Programmausfuehrung nicht zu lang dauert
			n += 50000000;
		}
		else
		{
			if(abs((sum-vergl)/vergl) < e*100)
			{
				n += 10000000000;
			}
			else
			{
				n *= 10000;
			} 
		}
		h=(b-a)/n;
		sum = 0;
		for(int i=1; i<n; i++)
		{
			sum+=h*f1(a-h/2+i*h);
		}
	}
	return n;
}
